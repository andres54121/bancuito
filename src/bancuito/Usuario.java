package bancuito;

public class Usuario {
    private int id;
    private String nombre;
    private String apellidoP;
    private String apellidoM;
    private int NIP;
    private int numTarjeta;
    private int fondosIni;
    private String Historial;

    public String getHistorial() {
        return Historial;
    }

    public void setHistorial(String Historial) {
        this.Historial = Historial;
    }

    public int getId() {
            return id;
    }
    public void setId(int id) {
            this.id = id;
    }
    public String getNombre() {
            return nombre;
    }
    public void setNombre(String nombre) {
            this.nombre = nombre;
    }
    public String getApellidoP() {
            return apellidoP;
    }
    public void setApellidoP(String apellidoP) {
            this.apellidoP = apellidoP;
    }
    public String getApellidoM() {
            return apellidoM;
    }
    public void setApellidoM(String apellidoM) {
            this.apellidoM = apellidoM;
    }
    public int getNIP() {
        return NIP;
    }
    public void setNIP(int NIP) {
        this.NIP = NIP;
    }
    public int getNumTarjeta() {
        return numTarjeta;
    }
    public void setNumTarjeta(int numTarjeta) {
        this.numTarjeta = numTarjeta;
    }
    public int getFondosIni() {
        return fondosIni;
    }
    public void setFondosIni(int fondosIni) {
        this.fondosIni = fondosIni;
    }

    @Override
    public String toString() {
        return "Usuario: \n" + 
                "id= " + id + 
                "\nNombre Completo= " + nombre + " " + apellidoP + " " + apellidoM + 
                "\nNIP= " + NIP + 
                "\nnumTarjeta= " + numTarjeta + 
                "\nFondos= " + fondosIni ;
    }
}

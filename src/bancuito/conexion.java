package bancuito;

import java.util.ArrayList;
import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

public class conexion {
    private ObjectContainer db = null;

    private void abrirRegistro() {
        db = Db4oEmbedded.openFile("RegistroUsuario");
    }

    private void cerrarConexion() {
        db.close();
    }

    public void insertarRegistro(Usuario u) {
        abrirRegistro();
        db.store(u);
        cerrarConexion();
    }
    
    public Usuario seleccionarUsuario(Usuario u) {
        abrirRegistro();
        ObjectSet resultado = db.queryByExample(u);
        Usuario usuario = (Usuario) resultado.next();
        cerrarConexion();
        return usuario;
    }
    
    public void actualizacionFondos(int id, int cantidad ){
        abrirRegistro();
        Usuario u = new Usuario();
        u.setId(id);
        ObjectSet resultado = db.queryByExample(u);
        
        Usuario preresultado = (Usuario) resultado.next();
        preresultado.setFondosIni(preresultado.getFondosIni() + cantidad);
        
        db.store(preresultado);
        cerrarConexion();
    }
}

package bancuito;

import java.util.Scanner;

public class cajero {
    conexion c = new conexion();
    Usuario usuario = new Usuario();
    Scanner te = new Scanner(System.in);
    
    int num;
    
    public boolean validacionTarjeta(int numT, int nip){
        usuario.setNumTarjeta(numT);
        usuario = c.seleccionarUsuario(usuario);
        if(nip==usuario.getNIP()){
            System.out.println(usuario.toString());
            num = numT;
            return true;            
        }        
        return false;
    }
    
    public boolean validacionFondos(int cantidad){
        Usuario usuarioA = new Usuario();
        usuarioA.setNumTarjeta(num);
        usuarioA = c.seleccionarUsuario(usuarioA);
        if(cantidad<=usuarioA.getFondosIni()){
            c.actualizacionFondos(usuarioA.getId(), (0-cantidad));
            return true;            
        }        
        return false;
    }
    
    public void deposito(int numt){
        System.out.println("Ingrese la cantidad a depositar ");
        int cantidad = te.nextInt();
        c.actualizacionFondos(usuario.getId(), cantidad);
        System.out.println("\nDeposito realizado con exito");
    }
    public void retiro(){
        System.out.println("Ingrese la cantidad a retirar");
        int cantidad = te.nextInt();
        if(validacionFondos(cantidad)==true){
            System.out.println("\nRetiro realizado con exito\n");
        }else{
            System.out.println("SALDO INSUFICIENTE");
            System.out.println("Desea intentarlo de nuevo?\nSI=1 NO=Cualquier Numero");
            int v =te.nextInt();
            if (v==1) {
                retiro();
            }
        }
    }
    
    public void verFondos(int numT){
        Usuario usuarioA = new Usuario();
        usuarioA.setNumTarjeta(numT);
        usuarioA = c.seleccionarUsuario(usuarioA);        
        System.out.println("\nFondos Actuales: " + usuarioA.getFondosIni());
    }
}
